CC=gcc

# Source files
SRC=$(shell ls *.c)
HDR=$(shell ls *.h)
OBJ=$(SRC:.c=.o)

# Linked libraries
LIB=-lncursesw -lm

# Include directories
INC=

# General compiler flags
FLG=-g -Wall -std=c99 -D_BSD_SOURCE

all: water

water: $(SRC) $(OBJ)
	$(CC) $(OBJ) $(FLG) $(INC) $(LIB) -o water

%.o: %.c $(HDR)
	$(CC) $(FLG) $(INC) $(LIB) -o $@ -c $<

clean:
	rm -f $(OBJ)

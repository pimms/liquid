#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <ncurses.h>
#include <sys/ioctl.h>
#include <signal.h>
#include <assert.h>

typedef struct
{
	float sat;		// Fluid saturation
	float xvel;
	float yvel;
} Voxel;

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)

int WORLDSZX = 0;
int WORLDSZY = 0;

Voxel world[256][256];

WINDOW* curswin;


float worldsat()
{
	float sum = 0.f;

	for (int x=0; x<WORLDSZX; x++)
	{
		for (int y=WORLDSZY-1; y>=0; y--)
		{
			sum += world[x][y].sat;
		}
	}

	return sum;
}

void draw()
{
	static const char *map = " 0123456789";// " .,oxi0OIX@#";
	static int frame = 0;
	static char strbuf[80];

	const int maplen = strlen(map);

	box(curswin, 0, 0);

	for (int x=0; x<WORLDSZX; x++)
	{
		for (int y=0; y<WORLDSZY; y++)
		{
			const int mapidx = floor(world[x][y].sat * (float)(maplen-1));
			move(y+1, x+1);
			addch(map[mapidx]);
		}
	}

	sprintf(strbuf, "World Liquid: %g", worldsat());
	move(WORLDSZY+2, 2);
	addstr(strbuf);

	sprintf(strbuf, "Frame: %d", ++frame);
	move(WORLDSZY+2, 25);
	addstr(strbuf);

	refresh();
}

bool validcoord(int x, int y)
{
	return (x >= 0 && x < WORLDSZX &&
			y >= 0 && y < WORLDSZY);
}

void leak(int x, int y)
{
	static const int leakorder[5][2] = {{0, 1}, {-1, 1}, {1, 1}, {-1, 0}, {1, 0}};

	Voxel *cur = &world[x][y];
	if (cur->sat <= 0.f)
		return;

	for (int i=0; i<5; i++)
	{
		Voxel *other = NULL;

		// Leak as much as possible straight down
		if (y+1 < WORLDSZY)
		{
			other = &world[x][y+1];
			float transfer = MAX(cur->sat - other->sat, 0.f);
			cur->sat -= transfer;
			other->sat += transfer;
		}

		// Spread the remainding fluid evenly
		for (int i=0; i<2 && cur->sat > 0.f; i++)
		{
			Voxel *left = NULL;
			Voxel *right = NULL;
			int dx;
			int dy;

			dx = leakorder[1 + i*2 + 0][0];
			dy = leakorder[1 + i*2 + 0][1];
			if (validcoord(x+dx, y+dy))
				left = &world[x+dx][y+dy];
			if (left && left->sat > cur->sat)
				left = NULL;

			dx = leakorder[1 + i*2 + 1][0];
			dy = leakorder[1 + i*2 + 1][1];
			if (validcoord(x+dx, y+dy))
				right = &world[x+dx][y+dy];
			if (right && right->sat > cur->sat)
				right = NULL;

			// Find out how much to leak into left & right
			if (left && right)
			{
				Voxel *high;
				Voxel *low;

				if (left->sat > right->sat)
				{
					high = left;
					low = right;
				}
				else
				{
					high = right;
					low = left;
				}

				// Even out "high" and "low", or vacate all excess liquid into "low"
				float bias = MIN(high->sat - low->sat, cur->sat);
				cur->sat -= bias;
				low->sat += bias;

				if (cur->sat > 0.f)
				{
					// Divide the remaining excess evenly between "low", "cur" and "high"
					assert(fabsf(high->sat - low->sat) < 0.001f);

					float divide = (cur->sat - high->sat) / 3.f;
					high->sat += divide;
					low->sat += divide;
					cur->sat -= 2.f * divide;
				}
			}
			else
			{
				// Even out either "left" or "right"
				other = (left ? left : (right ? right : NULL));
				if (!other)
					return;

				if (other->sat < cur->sat)
				{
					float transfer = cur->sat - other->sat;
					other->sat += transfer;
					cur->sat -= transfer;
				}
			}

		}
	}
}

void simulate()
{
	for (int x=0; x<WORLDSZX; x++)
	{
		for (int y=WORLDSZY-1; y>=0; y--)
		{
			leak(x, y);
		}
	}
}

void cinit()
{
	struct winsize ws;
	if (ioctl(0, TIOCGWINSZ, &ws) < 0)
	{
		perror("Failed to retrieve term size\n");
		exit(1);
	}

	WORLDSZX = ws.ws_col - 2;
	WORLDSZY = ws.ws_row - 4;
	printf("Window size: %d x %d\n", WORLDSZX, WORLDSZY);

	if ((curswin = initscr()) == NULL)
	{
		perror("Curses initialization failed\n");
		exit(1);
	}

	raw();
	timeout(0);
}

void init()
{
	const int width = WORLDSZX / 5;
	const int height = WORLDSZY / 5;

	for (int x=(WORLDSZX/2-width/2); x<(WORLDSZX/2+width/2); x++)
	{
		for (int y=(WORLDSZY/3-height/2); y<(WORLDSZY/3+height/2); y++)
		{
			world[x][y].sat = 1.f;
		}
	}

	for (int x=0; x<5 && x<WORLDSZX; x++)
	{
		for (int y=0; y<5 && y < WORLDSZY; y++)
		{
			world[x][y].sat = (float)x*0.1f + (float)y*0.1f;
		}
	}
}

int main()
{
	cinit();
	init();

	while (true)
	{
		simulate();
		draw();
		usleep(10000);

		char ch = getch();
		if (ch == 'q')
			break;
	}


	timeout(5);
	getch();

	endwin();

	return 0;
}
